import { Modal, ModalBody, ModalFooter, ModalHeader } from './components/libModal';
import {useState} from "react";

function App() {
    const [showModal, setShowModal] = useState(false);

    return (
        <div>
            <button>
                <span className="text" onClick={() => setShowModal(true)}>Click on me</span>
            </button>
            <Modal
                show={showModal}
                setShow={setShowModal}
            >
                <ModalHeader>
                    <h2>Modal Example</h2>
                </ModalHeader>
                <ModalBody>
                    <p style={{ textAlign: 'justify' }}>
                        Proin dolor mi, finibus a ipsum et, vulputate cursus mauris. Donec et tristique quam. Vivamus imperdiet maximus odio. Curabitur scelerisque arcu ultricies, commodo sem id, aliquet lacus. Nulla quam dolor, facilisis eget finibus dapibus, commodo at nunc. Cras eget sodales leo, eu fermentum magna. Nulla non neque ut quam ultrices aliquam. Phasellus cursus mi at dictum condimentum.
                    </p>
                </ModalBody>
                <ModalFooter>
                    <a onClick={() => setShowModal(false)}>
                        Close
                    </a>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default App;
