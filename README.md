# basic-react-dropdown

A small library to display a modal

## Installation
```
npm i modal-react-op
```

## Usage
```jsx
import { Modal, ModalBody, ModalFooter, ModalHeader } from './libModal';
import {useState} from "react";

const [showModal, setShowModal] = useState(false);

<button className="text" onClick={() => setShowModal(true)}>button</button>

<Modal 
    show={showModal}
    setShow={setShowModal}
>

    <ModalHeader>
        <h2>Modal Example</h2>
    </ModalHeader>
    
    <ModalBody>
        <p>
            Proin dolor mi, finibus a ipsum et, vulputate cursus mauris. Donec et tristique quam. Vivamus imperdiet maximus odio. Curabitur scelerisque arcu ultricies, commodo sem id, aliquet lacus. Nulla quam dolor, facilisis eget finibus dapibus, commodo at nunc. Cras eget sodales leo, eu fermentum magna. Nulla non neque ut quam ultrices aliquam. Phasellus cursus mi at dictum condimentum.
        </p>
    </ModalBody>
    
    <ModalFooter>
        <a onClick={() => setShowModal(false)}>
            Close
        </a>
    </ModalFooter>

</Modal>
```

## API
### Props
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>show</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
        <tr>
          <td>setShow</td>
          <td>Boolean</td>
          <td>Use to change the value</td>
        </tr>
        <tr>
          <td>onChange</td>
          <td>function</td>
          <td>Launch function when user select value</td>
        </tr>
        <tr>
          <td>hideCloseButton</td>
          <td>Bool</td>
          <td>Use to hide the close button</td>
        </tr>
    </tbody>
</table>

## Development
```
npm run start
```